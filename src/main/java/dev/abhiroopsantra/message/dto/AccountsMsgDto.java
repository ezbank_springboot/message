package dev.abhiroopsantra.message.dto;

/**
 * DTO class for Accounts message
 *
 * @param accountNumber
 * @param name
 * @param email
 * @param mobileNumber
 */
public record AccountsMsgDto(Long accountNumber, String name, String email, String mobileNumber) {

}
